import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

export default ({ article }) => {
  const days = [
    "Senin",
    "Selasa",
    "Rabu",
    "Kamis",
    "Jum'at",
    "Sabtu",
    "Minggu",
  ];

  const [expiredAt, setExpiredAt] = useState(0);
  useEffect(() => {
    const index = days.findIndex((day) => day == article.published_at);
    setExpiredAt((index + article.expired_at) % 7);
  }, [article]);

  return (
    <Link
      to={"/detail/" + article.id}
      className="border-2 rounded-lg border-purple-200 w-full px-2 py-2 hover:bg-purple-100"
    >
      <img src={article.image} className="w-full " alt="" />
      <div className="mt-2 px-5">
        <p className="font-base font-bold text-purple-900">{article.title}</p>
        <p className="font-sm  ">Published At: {article.published_at}</p>
        <p className="font-sm ">Expired At: {days[expiredAt]} </p>
      </div>
    </Link>
  );
};
