import React, { useEffect, useState } from "react";
import Article from "./Article";

export default () => {
  const [articles, setArticles] = useState([]);
  useEffect(() => {
    fetch("https://mocki.io/v1/f18c2b81-0b4e-48b8-a6da-cd369e866c41")
      .then((res) => res.json())
      .then((res) => {
        console.log(res.data);
        setArticles(res.data);
      });
  }, []);

  return (
    <React.Fragment>
      <div className="py-5 px-8 bg-purple-500 text-white text-lg font-semibold">
        Article App
      </div>
      <div className="content px-12 my-5">
        <h2 className="text-lg font-semibold">List Article</h2>
        <div className="grid grid-cols-3 gap-4 mt-5">
          {articles.map((article) => (
            <Article article={article} />
          ))}
        </div>
      </div>
    </React.Fragment>
  );
};
