import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Detail from "./Detail";
import Home from "./Home";
function App() {
  return (
    <Router>
      <Switch>
        <Route path="/" exact>
          <Home />
        </Route>

        <Route path="/detail/:id" exact>
          <Detail />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
