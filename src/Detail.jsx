import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
export default () => {
  const { id } = useParams();
  const [article, setArticle] = useState([]);
  useEffect(() => {
    fetch("https://mocki.io/v1/f18c2b81-0b4e-48b8-a6da-cd369e866c41")
      .then((res) => res.json())
      .then((res) => {
        setArticle(res.data.find((data) => data.id == id));
      });
  }, []);
  return (
    <React.Fragment>
      <div className="py-5 px-8 bg-purple-500 text-white text-lg font-semibold">
        Article App
      </div>
      <div className="content px-12 my-5">
        <h2 className="text-lg font-semibold">{article?.title}</h2>
        <div className="grid grid-cols-1">
          <img src={article?.image} className="w-full" alt="" />
          <div>
            <h2 className="text-lg font-semibold">Content</h2>
            <div
              className="text-sm"
              dangerouslySetInnerHTML={{ __html: article?.content }}
            ></div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
